var mydata = JSON.parse(data);
var contenido = document.querySelector("#contenido");
var selector = document.getElementById("select");

for (let valor of mydata) {
    let option = document.createElement("option");
    option.value = valor.Nombre;
    option.innerText = valor.Nombre;
    selector.appendChild(option);
}

function mostrar(nombre) {
    contenido.innerHTML = "";
    for (let valor of mydata) {
        if (nombre.value == valor.Nombre) {
            contenido.innerHTML += `
                        <tr>
                            <td>${valor.Cédula}</td>
                            <td scope="row">${valor.Nombre}</td>
                            <td>${valor.Dirección}</td>
                            <td>${valor.Teléfono}</td>
                            <td>${valor.Correo}</td>
                            <td>${valor.Curso}</td>
                            <td>${valor.Paralelo}</td>
                        </tr>
                    `;
        }
    }
}
